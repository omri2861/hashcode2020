"""
This file contains the HashCode contest's context object
"""

from queue import Queue
import warnings
from typing import List, Dict

from library import Book, Library


class Context:
    def __init__(self, time: int, total_books: int, book_map: Dict[int, Book], libraries: List[Library]):
        self.time = time
        self.total_books = total_books
        self.book_map = book_map
        self.libraries = libraries

        # Initialize state
        self.signup_day = 0
        self.signup_order = Queue()
        self.scanned_books = []

    def __str__(self):
        result = "#" * 8 + "Game" + "#" * 8 + "\n"
        result += f" * This game lasts {self.time} days.\n"
        result += f" * This game has a total of {self.total_books} books" \
                  f".\n"
        result += f" * This game has {len(self.libraries)} libraries.\n"
        for library in self.libraries:
            result += f" -- {library}\n"

        return result

    def signup_library(self, lib_id: int, books: List[int]):
        signup_time = self.libraries[lib_id].signup_time
        if self.signup_day + signup_time > self.time:
            warnings.warn(f"Cannot signup library {lib_id}: Not enough days")
            return

        if len(books) == 0:
            warnings.warn(f"No books to scan in library {lib_id}. Hopefully, you'll add them later.")

        self.signup_day += signup_time
        self.signup_order.put(lib_id)
        self.libraries[lib_id].sign_up(self.time - self.signup_day)
        self.scan_books(lib_id, books)
        print(f"Signed up library {lib_id}, finished in day {self.signup_day}/{self.time}.")

    def scan_books(self, lib_id: int, books: List[int]):
        library = self.libraries[lib_id]
        if library is None or not library.is_signed_up:
            warnings.warn(f"Trying to scan books from an unsigned librray {lib_id}. Skipping.")
            return

        for book_id in books:
            if book_id in self.scanned_books:
                continue
            book_scanned = library.scan_book(book_id)
            if book_scanned:
                self.scanned_books.append(book_id)

    def calculate_current_score(self):
        return sum([self.book_map[book_id].book_score for book_id in self.scanned_books])

    def submit(self, dest_filename):
        lines = [str(self.signup_order.qsize())]

        while self.signup_order.qsize() > 0:
            lib_id = self.signup_order.get_nowait()
            library = self.libraries[lib_id]
            if library.is_signed_up:
                lines.append(f"{lib_id} {len(library.scanned_books)}")
                lines.append(" ".join([str(book_id) for book_id in library.scanned_books]))

        with open(dest_filename, 'wb') as dest:
            for line in lines:
                dest.write(line.encode("ascii") + b"\n")
