# Google HashCode 2020

Hi there!
This is my solution for Google's HashCode 2020 qualification exercise.

Basically, I worked on creating an infrastructure first, which will provide an easy
API for parsing the settings, signing up libraries and scanning books.

Then, I worked on thinking about strategies to handle the task and implemented them.

Unfortunately, I ended up with a low score...
Here are some of my conclusions from this exercise:

### Conclusions

1. Make more time, be more focus. Don't be busy with other stuff if you have a very limited amount of time.
2. Plan your time. Since I wasn't too focus, I wasn't paying attention to how much time I have left for strategies-
 eventually I only had about half an hour for that. Too bad, because I was really close to make a good solution.
 Next time, I should divide my time into infrastructure and strategy. Maybe first plan the strategy.
3. Learn how to write competitive code. I guess that's a whole genre in itself, and there are great tricks which can
 help you write fast code online.
4. Since I just went ahead and started writing code, I didn't think for a second about a strategy. If I would, I
 might've realized that I put too much time and effort in letting the user scan books, specefing which book each time,
 while there is no true reason for that- the best solution is to always simply scan them by the highest score, as much
 as you can (as long as you didn't scan them already). 

### Navigating the code

* The main code resides in the `hashcode.py` file. It doesn't do too much, mainly just prepare the context object and
 write the output to a file.
* Most of the logic is found in the `Context` & `Library` objects, found in the `context.py` and `library.py` files
 respectively.
* The strategies to solve the problem are found in the `strategies.py` file. There is a good explanation there for how
 to define them. 

### Running The Project

1. Make sure you have a folder in the project's root called `outputs`
2. Run the project: `Usage: py -3 hashcode.py <strategy>`. Make sure to pick a valid strategy from the `strategies.py`
 file.
