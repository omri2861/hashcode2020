"""
This file contains strategies for signing up books.
A strategy is simply a function which takes a new context object, then signs up books and libraries,
returning nothing.
"""


def do_nothing(context):
    """
    does nothing to the context and returns.
    """
    pass


def a_example(context):
    """
    The strategy for file a
    """
    context.signup_library(0, [2, 3, 4])
    context.signup_library(1, [])
    context.scan_book(1, [2])


def b_read_on(context):
    """
    The strategy for file b
    """
    context.signup_library(0, [2, 3, 4])
    context.signup_library(1, [])
    context.scan_book(1, [2])


def register_all(context):
    """
    Simply registers all, by order
    """
    for library in context.libraries:
        if library is None:
            continue
        books_to_register = [book.book_id for book in library.books]
        context.signup_library(library.library_id, books_to_register)


def score_average(context):
    """
    The strategy is to sort libraries to sign up by their books' average score, then scan all of their books,
    starting from the ones with the highest scores, going down to the lowest.
    """
    libs = [library for library in context.libraries if library is not None]
    libs = sorted(libs, key=lambda library: library.average_score, reverse=True)

    for library in libs:
        books = [book.book_id for book in sorted(library.books, key=lambda book: book.book_score, reverse=True)]
        context.signup_library(library.library_id, books)


def books_per_day(context):
    """
    The strategy os to sort libraries to sign up by the fastest shipping time, then scan all of their books,
    starting from the ones with the highest scores, going down to the lowest.
    """
    libs = [library for library in context.libraries if library is not None]
    libs = sorted(libs, key=lambda library: library.shipping_pace, reverse=True)

    for library in libs:
        books = [book.book_id for book in sorted(library.books, key=lambda book: book.book_score, reverse=True)]
        context.signup_library(library.library_id, books)


strategies = {
    "do_nothing": do_nothing,
    "a_example": a_example,
    "b_read_on": b_read_on,
    "register_all": register_all,
    "score_average": score_average,
    "books_per_day": books_per_day
}
