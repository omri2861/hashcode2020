"""
This file contains the objects which I will use to map the files.
"""
import warnings
from dataclasses import dataclass
from typing import List


@dataclass
class Book:
    book_id: int
    book_score: int


@dataclass(init=False)
class Library:
    def __init__(self, library_id: int, book_count: int, signup_time: int, shipping_pace: int, books: List[Book]):
        # TODO: Parse from lines
        self.library_id = library_id
        self.book_count = book_count
        self.signup_time = signup_time
        self.shipping_pace = shipping_pace
        self.books = books

        # Initialize state:
        self.scanned_books = []  # Which book IDs were scanned
        self.is_signed_up = False  # Whether the library is signed up or not
        self.books_to_scan = 0

        # Calculate some data:
        self.average_score = sum([book.book_score for book in self.books]) / len(self.books)

    def sign_up(self, time_left):
        self.is_signed_up = True
        self.books_to_scan = time_left * self.shipping_pace

    def scan_book(self, book_id: int):
        """
        Tells the library to ship a book for scanning
        :param book_id: The book id to ship
        :return: None
        """
        if self.books_to_scan < 1:
            warnings.warn(f"Cannot scan books for library {self.library_id}: can't make it for the deadline.")
            return False

        if book_id not in [book.book_id for book in self.books]:
            warnings.warn(
                f"Error: Trying to scan book f{book_id} which is not in library {self.library_id}. Skipping")
            return False

        self.scanned_books.append(book_id)
        return True

    def __repr__(self):
        result = f"Library(id={self.library_id}, signup_time={self.signup_time}, shipping_pace:{self.shipping_pace}," \
                 f" books:{[book.book_id for book in self.books]})"

        return result

    def __str__(self):
        result = f"LIB{self.library_id}: {self.signup_time} to register, {self.shipping_pace} books per day, " \
                 f"{self.book_count} books, with an average score of {self.average_score}."

        return result
