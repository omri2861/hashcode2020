"""
This is my hashcode's main file!
It solves the problem and saves the solution into text files :)1
"""

import sys
import os

from parse_input import *
from strategies import strategies

TEXT_EXTENSION = ".txt"


def play(filename: str, strategy_name: str):
    """
    Attempts to play a game in the given setting, using the given strategy.
    The result is saved to a file in the output folder, in the format: '<input_file>_<score>_<strategy>.txt'. This
    format allows easily analyze which strategy gave the best score for which setting.
    :param filename: The file which describes the books and libraries
    :param strategy_name: The name of the strategy to pick for this game
    :return: None
    """
    output_file_name = filename
    if output_file_name.endswith(TEXT_EXTENSION):
        output_file_name = output_file_name.rstrip(TEXT_EXTENSION)

    with open(f"inputs\\{filename}") as src:
        context = parse_context(src.readlines())

    print(context)
    strategies[strategy_name](context)

    score = context.calculate_current_score()
    output_file_name = "_".join([output_file_name, str(score), strategy_name]) + TEXT_EXTENSION

    print(f"######## FILE: {output_file_name} | SCORE: {score} ########")
    context.submit(f"outputs\\{output_file_name}")


def main():
    if len(sys.argv) < 2:
        print("Usage: python hashcode.py <strategy>")
        return

    strategy_name = sys.argv[1]
    for filename in os.listdir("inputs"):
        play(filename, strategy_name)


if __name__ == "__main__":
    main()
