import warnings

from library import Book, Library
from context import Context
from typing import List

LINE_SEP = "\n" + ("#" * 16)


def separate_line(function):
    def line_separating_function(line):
        result = function(line)
        print(LINE_SEP)
        return result

    return line_separating_function


def line_to_integers(line: str):
    """
    :param line: A single line read from an input file
    :return: A list of integers, parsed from that line of input
    """
    line = line.rstrip("\n")
    return [int(num) for num in line.split(" ")]


@separate_line
def parse_first_line(line: str):
    books, libraries, days = line_to_integers(line)
    print(f"There are {books} books, in {libraries} different libraries, and I have {days} days to scan them.")


@separate_line
def parse_second_line(line: str):
    scores = line_to_integers(line)
    result = [Book(book_id, score) for book_id, score in enumerate(scores)]
    print("Book scores are:")
    for book in result:
        print(f"\t{book}")

    return result


def parse_book_map(line: str):
    scores = line_to_integers(line)
    return {book_id: Book(book_id, score) for book_id, score in enumerate(scores)}


def parse_library(lines: List[str], book_map, library_id):
    if len(lines) != 2:
        warnings.warn(f"NOTE: Getting incorrect number of lines for a single library ({len(lines)})")
        return

    book_count, signup_time, shipping_pace = line_to_integers(lines[0])
    book_ids = line_to_integers(lines[1])
    books = [book_map[book_id] for book_id in book_ids]

    return Library(library_id, book_count, signup_time, shipping_pace, books)


def parse_context(lines: List[str]):
    books, libraries, days = line_to_integers(lines[0])
    book_map = parse_book_map(lines[1])
    libraries = []
    library_id = 0
    for i in range(2, len(lines), 2):
        libraries.append(parse_library(lines[i:i + 2], book_map, library_id))
        library_id += 1

    return Context(days, books, book_map, libraries)
